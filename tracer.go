package trace

import (
	"fmt"
	"io"
)

// Tracer はコードないでの出来事を記録できるObjectを表すインターフェースです
type Tracer interface {
	Trace(...interface{})
}

type tracer struct {
	out io.Writer
}

func (t *tracer) Trace(a ...interface{}) {
	t.out.Write([]byte(fmt.Sprint(a...)))
	t.out.Write([]byte("\n"))
}

// New はiwを指定して、Traceを使用できるようにする
func New(w io.Writer) Tracer {
	return &tracer{out: w}
}

type nilTracer struct {
}

func (t *nilTracer) Trace(a ...interface{}) {
}

// Off はTraceメソッドの呼び出しを無視するTracerを返します
func Off() Tracer {
	return &nilTracer{}
}
